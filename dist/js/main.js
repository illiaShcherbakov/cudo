(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

$(document).ready(function () {

  //toggle menu
  $('.menu-toggle').click(function () {
    $('body').toggleClass('fix');
    $('.nav').toggleClass('active');
  });

  //hide and reveal labels of inputs on focus
  $('#form input, #form textarea').focus(function () {
    $('#form label[for="' + $(this).attr('id') + '"]').hide();
  });

  $('#form input, #form textarea').on('focusout', function () {
    if ($(this).val().length == 0) {
      $('#form label[for="' + $(this).attr('id') + '"]').show();
    }
  });

  //hide and reveal labels of inputs depending on whether filled or not
  $('#form input').each(function () {
    if ($(this).val().length > 0) $('#form label[for="' + $(this).attr('id') + '"]').hide();
    $(this).change(function () {
      if ($(this).val().length > 0) $('#form label[for="' + $(this).attr('id') + '"]').hide();else $('#form label[for="' + $(this).attr('id') + '"]').show();
    });
  });

  //Change tabs in portfolio section
  $('.switch-btn').on('click', function () {
    var idSection = $(this).attr('data-open');
    console.log(idSection);
    $('.portfolio-items.active').addClass('fadeOut').removeClass('fadeIn active');
    $('[data-section="' + idSection + '"]').removeClass('fadeOut').addClass('active fadeIn');
  });
});

},{}]},{},[1]);
