$(document).ready(function() {

  //toggle menu
  $('.menu-toggle').click(function() {
    $('body').toggleClass('fix');
    $('.nav').toggleClass('active');
  });

  //hide and reveal labels of inputs on focus
  $('#form input, #form textarea').focus(function() {
    $('#form label[for="' + $(this).attr('id') + '"]').hide();
  })

  $('#form input, #form textarea').on('focusout', function() {
    if ( $(this).val().length == 0 ) {
      $('#form label[for="' + $(this).attr('id') + '"]').show();
    }
  })

  //hide and reveal labels of inputs depending on whether filled or not
  $('#form input').each(function() { if($(this).val().length > 0) $('#form label[for="' + $(this).attr('id') + '"]').hide();
    $(this).change(function() {
      if($(this).val().length > 0)
        $('#form label[for="' + $(this).attr('id') + '"]').hide();
      else
        $('#form label[for="' + $(this).attr('id') + '"]').show();
    });
  });

  //Change tabs in portfolio section
  $('.switch-btn').on('click', function() {
    let idSection = $(this).attr('data-open');
    console.log(idSection);
    $('.portfolio-items.active').addClass('fadeOut').removeClass('fadeIn active');
    $('[data-section="' + idSection + '"]').removeClass('fadeOut').addClass('active fadeIn');
  })

})
